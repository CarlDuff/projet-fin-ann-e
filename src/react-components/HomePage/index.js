import React from 'react';
import ProductList from '../Product/ProductList';
import Firebase from 'firebase';

class HomePage extends React.Component {
  constructor() {
    super();
    
    //POUR AVOIR UN ARRAY DERIERE LE STATE
    //getInitialState = function() {
    //  productList : {
    //                   product : 1,
    //                   product : 2
    //                },
    //  
    //}
    //appel : this.state.productList[0];

    this.state = {
      productList: []
    }
  }

  componentWillMount() {
    var firebaseRef = new Firebase('https://codehunt-one.firebaseio.com/');
    firebaseRef.child("products").on('value', (snapshot) => {
      var products = snapshot.val();

      this.setState({
        productList: [products]
      })
    });
  }

  render() {
    return (
      <section>
        <header>
          <img src="img/banner.jpeg" width="100%" />
        </header>

        <section>
          <section className="container">
              {
                this.state.productList
                ?
                <ProductList productList={this.state.productList}/>
                :
                null
              }

          </section>
        </section>
      </section>
    );
  }
}

export default HomePage;
